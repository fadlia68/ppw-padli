from django.urls import include, path
from .views import index, hasil

urlpatterns = [
    path('', index, name='index'),
    path('hasil/', hasil),
]
