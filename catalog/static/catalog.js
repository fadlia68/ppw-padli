$("#box").keyup( function() {
    var x = $("#box").val()
    $("#init").empty();
    if (x != '') {
        $.ajax({
            url: 'hasil/?q=' + x,
        success: function(hasil) {

            var items = hasil.items;
            $("#listBook").empty();
            $("#listBook").append("<tr><th>Title</th><th>Author</th><th>Publisher</th><th>Preview</th></tr>");
            
            for (i = 0; i < items.length; i++) {
                var title = items[i].volumeInfo.title;
                var author = items[i].volumeInfo.authors;
                var publisher = items[i].volumeInfo.publisher;
                var preview = items[i].volumeInfo.previewLink;
                if (author == undefined) {
                    author = "-"
                }
                if (publisher == undefined) {
                    publisher = "-"
                }
                $("#listBook").append("<tr><td>" + title + "</td><td>" + author + "</td><td>" + publisher + "</td><td><a href='" + preview + "'>here</a></td></tr>");
            }
        }
        });
    }

});