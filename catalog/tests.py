from django.test import Client, TestCase, LiveServerTestCase, tag
from django.urls import resolve
from selenium import webdriver
from .views import index, hasil

class CatalogUnitTest(TestCase):
    
    def test_activity_url_is_exist(self):
        response = Client().get('/catalog/')
        self.assertEqual(response.status_code, 200)

    def test_activity_using_index_func(self):
        found = resolve('/catalog/')
        self.assertEqual(found.func, index)

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

@tag('functional')
class CatalogFunctionalTest(FunctionalTestCase):
    def test_ajax_call_catalog(self):
        self.selenium.get(f'{self.live_server_url}/catalog/')
        box = self.selenium.find_element_by_tag_name('input')
        box.send_keys('coba')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertIn('Book Catalog', html.text)
