from django.shortcuts import render
from django.http import JsonResponse
import urllib.request
import json

def index(request):
    return render(request, 'catalog.html')

def hasil(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + '+'.join(request.GET['q'].split())
    web = urllib.request.urlopen(url)
    data2 = json.loads(web.read())
    return JsonResponse(data2, safe=False)