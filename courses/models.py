from django.db import models

# Create your models here.

class Course(models.Model):
    name = models.CharField(max_length=100, default='mata kuliah x')
    teacher = models.CharField(max_length=50, default='bapa/ibu dosen')
    credits = models.CharField(max_length=2, default='0')
    year = models.CharField(max_length=30, default='tahun ini')
    room = models.CharField(max_length=10, default='fasilkom')
    desc = models.CharField(max_length=200, default='seru parah')
