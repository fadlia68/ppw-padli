from django import forms
from .models import Course

class Input_Form(forms.ModelForm):
	class Meta:
		model = Course
		fields = ['name', 'teacher', 'credits', 'year', 'room', 'desc']
	error_messages = {
		'required' : 'Please Type'
	}
	input_nam = {
		'type' : 'text',
		'placeholder' : 'Ex: PePeWe'
	}
	input_tea = {
		'type' : 'text',
		'placeholder' : 'Mr/Ms'
	}
	input_cre = {
		'type' : 'text',
		'placeholder' : '1-6'
	}
	input_yea = {
		'type' : 'text',
		'placeholder' : '20xx / 20xx'
	}
	input_roo = {
		'type' : 'text',
		'placeholder' : 'Gmeet/zoom'
	}
	input_des = {
		'type' : 'text',
		'placeholder' : 'what is it'
	}
	name = forms.CharField(label='Name', required=True, max_length=100, widget=forms.TextInput(attrs=input_nam))
	teacher = forms.CharField(label='Teacher', required=True, max_length=50, widget=forms.TextInput(attrs=input_tea))
	credits = forms.CharField(label='Credits', required=True, max_length=2, widget=forms.TextInput(attrs=input_cre))
	year = forms.CharField(label='Year', required=True, max_length=30, widget=forms.TextInput(attrs=input_yea))
	room = forms.CharField(label='Room', required=True, max_length=10, widget=forms.TextInput(attrs=input_roo))
	desc = forms.CharField(label='Description', required=True, max_length=200, widget=forms.TextInput(attrs=input_des))
