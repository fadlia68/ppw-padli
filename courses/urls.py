from django.urls import include, path
from .views import index, add, savename, delete, detail

urlpatterns = [
    path('', index, name='index'),
    path('add/', add, name='add'),
    path('add/savename', savename, name='savename'),
    path('delete/<id>', delete, name='delete'),
    path('detail/<id>', detail, name='detail')
]
