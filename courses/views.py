from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import Course

def index(request):
    response = {}
    courses = Course.objects.all()
    response['courses'] = courses
    return render(request, 'main/courses.html', response)

def add(request):
    response = {'form' : Input_Form}
    return render(request, 'main/add.html', response)

def savename(request):
    form = Input_Form(request.POST)
    if (request.method == 'POST' and form.is_valid):
        form.save()
    return HttpResponseRedirect('/courses')

def detail(request, id):
    obj = Course.objects.get(id=id)
    response = {'course' : obj}
    return render(request, 'main/detail.html', response)

def delete(request, id):
    Course.objects.get(id=id).delete()
    return HttpResponseRedirect('/courses')