from django.contrib import admin
from courses.models import Course
from activity.models import Person, Activity
# Register your models here.

admin.site.register(Course)
admin.site.register(Activity)
admin.site.register(Person)
