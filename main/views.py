from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.utils.safestring import mark_safe
from django.contrib.auth.models import User

def home(request):
    return render(request, 'main/home.html')

def details(request):
    return render(request, 'main/details.html')

def logIn(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else :
            messages.error(request,'please enter the correct username and password')
    return render(request, 'main/login.html')

def logOut(request):
    logout(request)
    return redirect('/')

def register(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')
        if password1 == password2:
            if len(password1) < 8:
                messages.error(request, 'please enter the password at least 8 character')
            else:
                try:
                    user= User.objects.get(username=username)
                    messages.error(request,'username is already taken. please choose the other username')
                except User.DoesNotExist:
                    user = User.objects.create_user(username, password=password1)
                    user.save()
                    login(request, user)
                    messages.success(request, 'well done. your account is saved')
        else:
            messages.error(request, 'please enter the same password for both field')
    return render(request, 'main/register.html')