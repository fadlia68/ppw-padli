var title = ["Description", "Activity", "Experience", "Achievement"]
var desc = ["Fasilkom Student", "Staff Quran FUKI and Asdos DDP1", "coming soon...", "become UI student"]

$(document).ready(function() {

  var judul = document.getElementsByClassName("accordion");
  var deskripsi = document.getElementsByClassName("isi");

  // initialize accordion
  init()
  
  // event click to expand accordion
  $(".accordion").click(function() {

    this.classList.toggle("active");
    var panel = "#" + this.parentElement.nextElementSibling.id;
    $(panel).slideToggle();

  });

  // event click to move up accordion
  $(".up").click(function() {
    var titleNow = this.nextElementSibling.innerHTML;
    var index = title.indexOf(titleNow);

    if (index != 0) {
      var temp = title[index - 1];
      title[index - 1] = title[index];
      title[index] = temp;

      var temp = desc[index - 1];
      desc[index - 1] = desc[index];
      desc[index] = temp;

      init()
    }

  });

  // event click to move down accordion
  $(".down").click(function() {
    var titleNow = this.previousElementSibling.innerHTML;
    var index = title.indexOf(titleNow);

    if (index != (title.length - 1)) {
      var temp = title[index + 1];
      title[index + 1] = title[index];
      title[index] = temp;

      var temp = desc[index + 1];
      desc[index + 1] = desc[index];
      desc[index] = temp;

      init()
    }

  });

  // initialize or re-initialize
  function init() {
    for (i = 0; i < judul.length; i++) {
      judul[i].innerHTML = title[i]
    }
    for (i = 0; i < judul.length; i++) {
      deskripsi[i].innerHTML = desc[i]
    }
  }

});

