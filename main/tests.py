from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from django.http import HttpRequest
from operator import index
from django.contrib.auth.models import User


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/details/')
        self.assertEqual(response.status_code, 200)

    def test_contains_html_page(self):
        response = self.client.get('/details/')
        html_response = response.content.decode('utf-8')
        self.assertIn('class="accordion"', html_response)
        self.assertIn('class="isi"', html_response)

    def test_login_success(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        response = self.client.post('/login/', data={'username': 'testuser', 'password': '12345'})
        self.assertEqual(response.status_code, 302)

    def test_login_fail(self):
        response = self.client.post('/login/', data={'username': 'testuser', 'password': '12345'})
        self.assertEqual(response.status_code, 200)

    def test_register_fail1(self):
        response = self.client.post('/register/', data={'username': 'testuser', 'password1': '12345', 'password2': '12345'})
        self.assertIn('please enter the password at least 8 character', response.content.decode('utf-8'))

    def test_register_fail2(self):
        response = self.client.post('/register/', data={'username': 'testuser', 'password1': '1234567890', 'password2': '12345678900'})
        self.assertIn('please enter the same password for both field', response.content.decode('utf-8'))
    
    def test_register_fail3(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        response = self.client.post('/register/', data={'username': 'testuser', 'password1': '1234567890', 'password2': '1234567890'})
        self.assertIn('username is already taken. please choose the other username', response.content.decode('utf-8'))

    def test_register_success(self):
        response = self.client.post('/register/', data={'username': 'testuser', 'password1': '1234567890', 'password2': '1234567890'})
        self.assertIn('well done. your account is saved', response.content.decode('utf-8'))

    def test_logout_success(self):
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        response = self.client.post('/login/', data={'username': 'testuser', 'password': '12345'})
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
