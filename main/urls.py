from django.urls import path
from .views import home, details, logIn, logOut, register

app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('details/', details, name='details'),
    path('login/', logIn, name='login'),
    path('logout/', logOut, name='logout'),
    path('register/', register, name='register'),
]
