from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Activity, Person
from .forms import Form_Activity, Form_Person

class ActivityUnitTest(TestCase):

    def test_activity_url_is_exist(self):
        response = Client().get('/activity/')
        self.assertEqual(response.status_code, 200)

    def test_activity_using_index_func(self):
        found = resolve('/activity/')
        self.assertEqual(found.func, index)

    def test_try_model(self):
        Activity.objects.create(name="Mabar")
        jumlah = Activity.objects.all().count()
        Person.objects.create(name="fadli")
        jumlah2 = Person.objects.all().count()
        self.assertEqual(jumlah, 1)
        self.assertEqual(jumlah2, 1)

    def test_activity_page_is_complete(self):
        request = HttpRequest()
        response = index(request)
        html = response.content.decode('utf8')
        self.assertIn('My Activities', html)
        self.assertIn('ADD', html)
    
    def test_activity_save_act_url_is_exist(self):
        response = Client().get('/activity/save_act')
        self.assertEqual(response.status_code, 302)

    def test_activity_save_person_url_is_exist(self):
        Activity.objects.create(name="mabar")
        response = Client().get('/activity/save_person/1')
        self.assertEqual(response.status_code, 302)
