from django import forms
from .models import Activity, Person

class Form_Activity(forms.ModelForm):
	class Meta:
		model = Activity
		fields = ['name']
	error_messages = {
		'required' : 'Please Type'
	}
	input_nam = {
		'type' : 'text',
		'placeholder' : 'new activity'
	}
	name = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs=input_nam))

class Form_Person(forms.ModelForm):
	class Meta:
		model = Person
		fields = ['name']
	
	error_messages = {
		'required' : 'Please Type'
	}

	input_nam = {
		'type' : 'text',
		'placeholder' : 'add person'
	}
	name = forms.CharField(label='', required=True, max_length=100, widget=forms.TextInput(attrs=input_nam))
