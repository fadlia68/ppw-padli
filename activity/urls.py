from django.urls import include, path
from .views import index, save_act, save_person

urlpatterns = [
    path('', index, name='index'),
    path('save_act', save_act, name='save_act'),
    path('save_person/<id>', save_person, name='save_person')
]
