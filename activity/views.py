from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Form_Activity, Form_Person
from .models import Activity, Person

# Create your views here.
def index(request):
    act = Activity.objects.all()
    person = Person.objects.all()
    response = {'form_act' : Form_Activity, 'form_person' : Form_Person, 'activities' : act, 'persons' : person}
    return render(request, 'main/activity.html', response)

def save_act(request):
    form = Form_Activity(request.POST)
    if (request.method == 'POST' and form.is_valid):
        form.save()
    return HttpResponseRedirect('/activity')

def save_person(request, id):
    act = Activity.objects.get(id=id)
    form = Form_Person(request.POST)
    persons = Person.objects.all()
    if (request.method == 'POST' and form.is_valid()):
        name = form.cleaned_data['name']
        ada = False
        for person in persons:
            if person.name == name:
                person.activity.add(act)
                ada = True
        if not ada:
            new_person = Person(name=name)
            new_person.save()
            new_person.activity.add(act)
    return HttpResponseRedirect('/activity')
