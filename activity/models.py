from django.db import models

# Create your models here.
class Activity(models.Model):
    name = models.CharField(max_length=20, default="Your Activity")

class Person(models.Model):
    name = models.CharField(max_length=20, default="Your Name")
    activity = models.ManyToManyField(Activity)